\select@language {english}
\select@language {english}
\contentsline {section}{Task}{2}{chapter*.1}
\contentsline {section}{Abstract}{3}{chapter*.2}
\contentsline {section}{List of abbreviations}{6}{chapter*.4}
\contentsline {section}{Introduction}{8}{chapter*.5}
\contentsline {chapter}{\numberline {1}Pipeline}{10}{chapter.1}
\contentsline {section}{\numberline {1.1}Sample preparation}{10}{section.1.1}
\contentsline {section}{\numberline {1.2}High-content screening}{11}{section.1.2}
\contentsline {section}{\numberline {1.3}Image analysis}{11}{section.1.3}
\contentsline {section}{\numberline {1.4}Machine learning}{12}{section.1.4}
\contentsline {chapter}{\numberline {2}Machine learning}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}Supervised learning}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}Classification and regression}{16}{section.2.2}
\contentsline {section}{\numberline {2.3}Uncertainty in regression}{19}{section.2.3}
\contentsline {section}{\numberline {2.4}Gaussian Processes}{20}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Prior}{20}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Characterization}{21}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Hyperparameters}{23}{subsection.2.4.3}
\contentsline {chapter}{\numberline {3}Active learning}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Active classification}{28}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Uncertainty sampling}{28}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Query by committee}{29}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Active regression}{30}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Committee members (CM)}{30}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Biggest variance (BV)}{32}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Greatest distance (GDI)}{33}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Out of bounds (OOB)}{33}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Uncertainty sampling (US)}{34}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Overall uncertainty sampling (OUC)}{34}{subsection.3.2.6}
\contentsline {chapter}{\numberline {4}Materials and methods}{36}{chapter.4}
\contentsline {section}{\numberline {4.1}Materials}{36}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Artificial data}{37}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Semi-artificial data}{37}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Real data}{38}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Algorithms}{40}{section.4.2}
\contentsline {chapter}{\numberline {5}Experimental results}{42}{chapter.5}
\contentsline {chapter}{\numberline {6}Software implementation}{45}{chapter.6}
\contentsline {chapter}{\numberline {7}Conclusion}{47}{chapter.7}
\contentsline {section}{Statement}{48}{chapter*.16}
\contentsline {section}{Acknowledgement}{49}{chapter*.17}
\contentsline {section}{Bibliography}{49}{chapter*.17}
\contentsline {chapter}{\numberline {8}Supplementary}{53}{chapter.8}
\contentsline {section}{\numberline {8.1}Source code snippets}{53}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Visualize GP and draw samples from the distribution}{53}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Modelling spots with 2D Gaussian pdf}{54}{subsection.8.1.2}
\contentsline {section}{\numberline {8.2}Result curves}{55}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Artificial data}{55}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}SIMCEP data}{59}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Lipid droplets on hepatocytes}{63}{subsection.8.2.3}
